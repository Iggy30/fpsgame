// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "Engine/EngineTypes.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "SHGame/Public/CoreTypes.h"
#include "SHGame/Public/Controllers/SHPlayerController.h"
#include "SHGame/Data/ItemDataStructs.h"
#include "WeaponDefault.generated.h"


UCLASS()
class SHGAME_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();


	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	FName MuzzleSocketName = "MuzzleSocket";
	
	UPROPERTY(VisibleAnywhere)
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAddicionalWeaponInfo AdditionalWeaponInfo;

	FOnClipEmptySignature OnClipEmpty;
	
	virtual void FireStart();
	virtual void FireStop();
	void ChangeClip();
	bool CanReload() const;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void MakeShot();

	bool GetPlayerViewPoint(FVector& ViewLocation,FRotator& ViewRotation) const;
	FVector GetMuzzleWorldLocation() const;

	ASHPlayerController* GetPlayerController() const;
	virtual bool GetTraceData(FVector& TraceStart,FVector& TraceEnd) const;

	void MakeHit(FHitResult& HitResult,const FVector& TraceStart,const FVector& TraceEnd);

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")\
	FAddicionalWeaponInfo DefaultAmmo{15,10,false};

	// patrons
	void DecriseAmmo();
	bool IsAmmoEmpty() const;
	//тукущая обойма пустая
	bool IsClipEmpty() const;
	// замена текуйщей обоймы на н овую
	void LogAmmo();



	void UpdateStateWeapon(EMovementState NewMovementWeaponState);
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	float TraceMaxDistance = 2000.0f;
		
	UPROPERTY()
	FVector ShootEndLocation = FVector(0);

	bool WeaponAiming = false;
	bool BlockFire = false;
		//Dispersion
	UPROPERTY()
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	USoundBase* NoAmmoSound;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	USoundBase* ClipsSound;

	
	UFUNCTION()
	void AmmoEmpty(USoundBase* NoAmmo);
	
	UFUNCTION()
	void SoundFXWeapon(UParticleSystem* FXFire,USoundBase* FireSound) const;

private:
	FAddicionalWeaponInfo CurrentAmmo;

	
};