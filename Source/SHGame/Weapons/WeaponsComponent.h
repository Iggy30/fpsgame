// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SHGame/Public/CoreTypes.h"
#include "WeaponsComponent.generated.h"


class AWeaponDefault;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHGAME_API UWeaponsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponsComponent();

	
	
	void FireStart();
	void FireEnd();
	void NextWeapon();
	void Reload();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UPROPERTY(EditDefaultsOnly,Category = "Weapon")
	FName WeaponEquipSocketName = "WeaponSocket";

	UPROPERTY(EditDefaultsOnly,Category = "Weapon")
	FName WeaponArmorySocketName = "ArmorySocket";

	UPROPERTY(EditDefaultsOnly,Category = "Animation")
	UAnimMontage* EquipAnimMontage;
	
	UPROPERTY(EditDefaultsOnly,Category = "Weapon")
	TArray<FWeaponData> WeaponData;

	
private:
	UPROPERTY()
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY()
	TArray<AWeaponDefault*> Weapons;

	UPROPERTY()
	UAnimMontage* CurrentReloadAnimMonrage = nullptr;
	UPROPERTY()
	UAnimMontage* CurrentFireAnimMonrage = nullptr;
	
	int32 CurrentWeaponIndex = 0;
	bool EquipAnimInProgress = false;
	bool ReloadAnimProgress = false;
	
	void SpawnWeapons();

	void AttachWeaponToSocket(AWeaponDefault* Weapon,USceneComponent* SceneComponent,const FName& SocketName);

	void EquipWeapon(int32 WeaponIndex);

	void PlayAnimMontage(UAnimMontage* Animation);
	void InitAnimations();
	void OnEquipFinished(USkeletalMeshComponent* MeshComponent);
	void OnReloadFinished(USkeletalMeshComponent* MeshComponent);


	bool CanFire() const;
	bool CanEquip() const;
	bool CanReload() const;

	void OnEmptyClip();
	void ChangeClip();
	

};
