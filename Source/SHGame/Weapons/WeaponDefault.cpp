// Fill out your copyright notice in the Description page of Project Settings.


#include "SHGame/Weapons/WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "Components//InventoryComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SkinnedMeshComponent.h"
#include "Engine/EngineTypes.h"
#include "Controllers/SHPlayerController.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Projectile/ProjectileDefault.h"
#include "SHGame/Data/ItemDataStructs.h"

DEFINE_LOG_CATEGORY_STATIC(LogBaseWeapon,All,All);

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent =CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Statick Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}


// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	check(SkeletalMeshWeapon);
	//check(DefaultAmmo.Bullets > 0 ,TEXT("Bullet count couldnt be less or equal zero"));
	//check(DefaultAmmo.Clips > 0 ,TEXT("Clips count couldnt be less or equal zero"));
	CurrentAmmo = DefaultAmmo;
	
}

void AWeaponDefault::MakeShot()
{
}


// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
}

void AWeaponDefault::AmmoEmpty(USoundBase* NoAmmo)
{
	if(NoAmmo)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), NoAmmoSound, ShootLocation->GetComponentLocation());
	}
}

void AWeaponDefault::SoundFXWeapon(UParticleSystem* FXFire, USoundBase* FireSound) const
{
		if (FireSound)
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, ShootLocation->GetComponentLocation());
		}
		if (FXFire)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXFire, ShootLocation->GetComponentTransform());
		}
	
}

ASHPlayerController* AWeaponDefault::GetPlayerController() const
{
	
	const auto Player = Cast<ACharacter>(GetOwner());
	if(!Player) return nullptr;

	return Player->GetController<ASHPlayerController>();

}



bool AWeaponDefault::GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const
{
	const auto Controller = GetPlayerController();
	if(!Controller) return false;
	
	Controller->GetPlayerViewPoint(ViewLocation,ViewRotation);
	return true;
}

FVector AWeaponDefault::GetMuzzleWorldLocation() const
{
	//return ShootLocation->GetComponentLocation();
	return SkeletalMeshWeapon->GetSocketLocation(MuzzleSocketName);
}

bool AWeaponDefault::GetTraceData(FVector& TraceStart,FVector& TraceEnd) const
{
	FVector ViewLocation;
	FRotator ViewRotation;
	if(!GetPlayerViewPoint(ViewLocation,ViewRotation)) return false;

	TraceStart = ViewLocation;
	const FVector ShootDiraction = ViewRotation.Vector();
	TraceEnd = TraceStart + ShootDiraction * TraceMaxDistance;
	return true;
}

void AWeaponDefault::MakeHit(FHitResult& HitResult,const FVector& TraceStart,const FVector& TraceEnd)
{
	if(!GetWorld()) return;
	
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());
	
	GetWorld()->LineTraceSingleByChannel(HitResult,TraceStart,TraceEnd,ECC_Visibility,CollisionParams);

}

void AWeaponDefault::DecriseAmmo()
{
	if(CurrentAmmo.Round ==0)
	{
		UE_LOG(LogBaseWeapon,Display,TEXT("-----Clip is Empty-----"));
		return;
	}
	
	CurrentAmmo.Round--;
	LogAmmo();

	if(IsClipEmpty() && !IsAmmoEmpty())
	{
			FireStop();
			OnClipEmpty.Broadcast();
	}
}

bool AWeaponDefault::IsAmmoEmpty() const
{	
	return !CurrentAmmo.Infinite && CurrentAmmo.Clips == 0 && IsClipEmpty();
		
}

bool AWeaponDefault::IsClipEmpty() const
{
	return CurrentAmmo.Round == 0;
}
void AWeaponDefault::FireStart()
{
	
}
void AWeaponDefault::FireStop()
{
	
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementWeaponState)
{
	BlockFire=false;
	switch(NewMovementWeaponState) {
	case EMovementState::Aim_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		WeaponAiming = false;
		BlockFire = true;
		
		break;
	case EMovementState::Crouch_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::CrouchAim_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::CrouchRun_State:
		WeaponAiming = false;
		BlockFire = true;
		break;
	default: ;
	}

}

void AWeaponDefault::ChangeClip()
{
	CurrentAmmo.Round = DefaultAmmo.Round;
	if(!CurrentAmmo.Infinite)
	{
		if(CurrentAmmo.Round ==0)
		{
			
			UE_LOG(LogBaseWeapon,Display,TEXT("-----No more CLip-----"));
			return;
		}
		CurrentAmmo.Clips--;
		
	}
	CurrentAmmo.Round = DefaultAmmo.Round;
	UE_LOG(LogBaseWeapon,Display,TEXT("-----Change CLip-----"));
}

bool AWeaponDefault::CanReload() const
{
	return CurrentAmmo.Round < DefaultAmmo.Round && CurrentAmmo.Clips >0;
}

void AWeaponDefault::LogAmmo()
{

	FString AmmoInfo = "Ammo: " + FString::FromInt(CurrentAmmo.Round) + "/";
	AmmoInfo += CurrentAmmo.Infinite ? "Infinite" : FString::FromInt(CurrentAmmo.Clips);
	UE_LOG(LogBaseWeapon,Display,TEXT("%s"),*AmmoInfo);
	
}

