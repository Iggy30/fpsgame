// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponsComponent.h"
#include "Components/ActorComponent.h"
#include "SHGame/Characters/SHGameCharacter.h"
#include "SHGame/Weapons/WeaponsComponent.h"
#include "Animations/EquipFinishedAnimNotify.h"
#include "Animations/SHReloadFinishedAnimNotify.h"
#include "Animations/AnimUtils.h"
#include "WeaponDefault.h"
#include "Dataflow/DataflowEngineUtil.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent,All,All);


// Sets default values for this component's properties
UWeaponsComponent::UWeaponsComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UWeaponsComponent::BeginPlay()
{
	Super::BeginPlay();

	//check(WeaponData.Num() == 2 ,TEXT("Our Character hold only 2 weapons"));
	// ...
	InitAnimations();
	SpawnWeapons();
	EquipWeapon(CurrentWeaponIndex);
}

void UWeaponsComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	CurrentWeapon = nullptr;
	for(auto Weapon : Weapons)
	{
		Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapon->Destroy();
	}
	Weapons.Empty();
	
	Super::EndPlay(EndPlayReason);
}


// Called every frame
void UWeaponsComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponsComponent::SpawnWeapons()
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!Character || !GetWorld()) return;

	for(auto OneWeaponData : WeaponData)
	{
		auto Weapon = GetWorld()->SpawnActor<AWeaponDefault>(OneWeaponData.WeaponClass);
		if(!Weapon) continue;
		Weapon->OnClipEmpty.AddUObject(this,&UWeaponsComponent::OnEmptyClip);
		Weapon->SetOwner(Character);
		Weapons.Add(Weapon);

		AttachWeaponToSocket(Weapon,Character->GetMesh(),WeaponArmorySocketName);
	}
	
	
	
}

void UWeaponsComponent::AttachWeaponToSocket(AWeaponDefault* Weapon, USceneComponent* SceneComponent,
	const FName& SocketName)
{

	if(!Weapon || !SceneComponent) return;
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget,false);
	Weapon->AttachToComponent(SceneComponent,AttachmentRules,SocketName);
	
	
}

void UWeaponsComponent::EquipWeapon(int32 WeaponIndex)
{
	if(WeaponIndex <0 || WeaponIndex >= Weapons.Num())
	{
		UE_LOG(LogWeaponComponent,Warning,TEXT("Invalid weapon index"));
		return;
	}
	
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!Character) return;

	if(CurrentWeapon)
	{
		CurrentWeapon->FireStop();
		AttachWeaponToSocket(CurrentWeapon,Character->GetMesh(),WeaponArmorySocketName);
	}
	CurrentWeapon = Weapons[WeaponIndex];
	//CurrentReloadAnimMonrage = WeaponData[WeaponIndex].ReloadAnimMontage;
	const auto CurrentWeaponData = WeaponData.FindByPredicate([&](const FWeaponData& Data)
	{
		return Data.WeaponClass == CurrentWeapon->GetClass();
	});
	CurrentReloadAnimMonrage = CurrentWeaponData ? CurrentWeaponData->ReloadAnimMontage: nullptr; 	
	AttachWeaponToSocket(CurrentWeapon,Character->GetMesh(),WeaponEquipSocketName);
	EquipAnimInProgress = true;
	PlayAnimMontage(EquipAnimMontage);
}

void UWeaponsComponent::PlayAnimMontage(UAnimMontage* Animation)
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!Character) return;

	Character->PlayAnimMontage(Animation);
}

void UWeaponsComponent::InitAnimations()
{
	
	auto EquipFinishedNotify = AnimUtils::FindNotifyByClass<UEquipFinishedAnimNotify>(EquipAnimMontage);
	if(EquipAnimMontage)
	{
		EquipFinishedNotify->OnNotified.AddUObject(this,&UWeaponsComponent::OnEquipFinished);
		
	}
	else
	{
		UE_LOG(LogWeaponComponent,Error,TEXT("Equip anim notify is forgotten to set"));
		checkNoEntry();
	}

	for(auto OnWeaponData : WeaponData)
	{
		auto ReloadFinishedAnimNotify = AnimUtils::FindNotifyByClass<USHReloadFinishedAnimNotify>(OnWeaponData.ReloadAnimMontage);
		if(!ReloadFinishedAnimNotify)
		{
			UE_LOG(LogWeaponComponent,Error,TEXT("Reload anim notify is forgotten to set"));
			checkNoEntry();
		}
		ReloadFinishedAnimNotify->OnNotified.AddUObject(this,&UWeaponsComponent::OnReloadFinished);
	}	
}

void UWeaponsComponent::OnEquipFinished(USkeletalMeshComponent* MeshComponent)
{

	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!Character || MeshComponent != Character->GetMesh()) return;
	
		EquipAnimInProgress = false;
		//UE_LOG(LogWeaponComponent,Display,TEXT("Equip finished"));

	
}

void UWeaponsComponent::OnReloadFinished(USkeletalMeshComponent* MeshComponent)
{
	
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!Character || MeshComponent != Character->GetMesh()) return;
	
	ReloadAnimProgress = false;
	//UE_LOG(LogWeaponComponent,Display,TEXT("Equip finished"));
}

bool UWeaponsComponent::CanFire() const
{
	return CurrentWeapon && !EquipAnimInProgress && !ReloadAnimProgress; 
}

bool UWeaponsComponent::CanEquip() const
{
	return !EquipAnimInProgress && !ReloadAnimProgress;
}

bool UWeaponsComponent::CanReload() const
{
	return CurrentWeapon
	&& !EquipAnimInProgress
	&& !ReloadAnimProgress
	&& CurrentWeapon->CanReload(); 
}

void UWeaponsComponent::OnEmptyClip()
{
	ChangeClip();
}

void UWeaponsComponent::ChangeClip()
{
	if(!CanReload()) return;
	CurrentWeapon->FireStop();
	CurrentWeapon->ChangeClip();
	ReloadAnimProgress = true;
	PlayAnimMontage(CurrentReloadAnimMonrage);
}

void UWeaponsComponent::FireStart()
{
	if(!CanFire()) return;
	CurrentWeapon->FireStart(),PlayAnimMontage(CurrentFireAnimMonrage);
}

void UWeaponsComponent::FireEnd()
{
	if(!CurrentWeapon) return;
	CurrentWeapon->FireStop();
}

void UWeaponsComponent::NextWeapon()
{
	if(!CanEquip()) return;
	CurrentWeaponIndex = (CurrentWeaponIndex +1) % Weapons.Num();
	EquipWeapon(CurrentWeaponIndex);
}

void UWeaponsComponent::Reload()
{
	if(!CanReload()) return;
	ReloadAnimProgress = true;
	PlayAnimMontage(CurrentReloadAnimMonrage);
}
