#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SHGameGameMode.generated.h"

UCLASS(minimalapi)
class ASHGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASHGameGameMode();
};



