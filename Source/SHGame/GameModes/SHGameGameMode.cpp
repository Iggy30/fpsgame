
#include "SHGame/GameModes/SHGameGameMode.h"
#include "SHGame/Characters/SHGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASHGameGameMode::ASHGameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
