

#include "SHGame/Characters/CharacterHealthComponent.h"
#include "GameFramework/Actor.h"


DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All)

// Sets default values for this component's properties
UCharacterHealthComponent::UCharacterHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

}


// Called when the game starts
void UCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	check(MaxHealth > 0);
	SetHealth(MaxHealth);
	
	AActor* ComponentOwner = GetOwner();
	if(ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UCharacterHealthComponent::OnTakeAnyDamageHandle);
	}
}


void UCharacterHealthComponent::SetHealth(float NewHealth)
{
	Health = FMath::Clamp(NewHealth,0.0f,MaxHealth);
	OnHealthChange.Broadcast(Health);
}

void UCharacterHealthComponent::OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <=0 || IsDead() || !GetWorld()) return;
	SetHealth(Health - Damage);
	
	if(IsDead())
	{
		OnDeath.Broadcast();
	}
	
	UE_LOG(LogHealthComponent,Display,TEXT("Damage: %f"),Damage);
}