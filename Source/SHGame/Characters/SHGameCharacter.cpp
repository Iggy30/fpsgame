
#include "SHGame/Characters/SHGameCharacter.h"
#include "UserInterface/MyHUD.h"
#include "Components/InventoryComponent.h"
#include "SHGame/Weapons/WeaponsComponent.h"

//Engine
#include "AsyncTreeDifferences.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "SHGame/Characters/CharacterHealthComponent.h"
#include "Components/TextRenderComponent.h"
#include "SHGame/Weapons/WeaponDefault.h"
#include "Components/SkeletalMeshComponent.h"
#include  "DrawDebugHelpers.h"
#include "SoundSubmixDefaultColorPalette.h"
#include "AssetTypeActions/AssetDefinition_SoundBase.h"
#include "Components/TimelineComponent.h"
#include "Engine/DamageEvents.h"
#include "SHGame/Weapons/WeaponDefault.h"
#include "SHGame/Weapons/Projectile/ProjectileDefault.h"
#include "SHGame/Public/Weapons/Projectile/Projectile_Grenade.h"
#include "World/PickUp.h"


DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ASHGameCharacter::ASHGameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	//GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	//Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	
	// Create a follow camera
	FPSCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FPSCamera"));
	FPSCamera->SetupAttachment(GetMesh(), "Head"); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	/*FPSCamera->SetRelativeLocation(FVector(0.f, 0.f, 0.0f));*/
	FPSCamera->bUsePawnControlRotation = true; // Camera does not rotate relative to arm

	AimingCameraTimeLine = CreateDefaultSubobject<UTimelineComponent>(TEXT("AimingCameraTimeLine"));
	DefaultCameraLocation = FVector{0.0f,0.0f,65.0f};
	AimingCameraLocation = FVector{175.0f,50.0f,55.0f};
	CameraBoom->SocketOffset = DefaultCameraLocation;

	CharHealth = CreateDefaultSubobject<UCharacterHealthComponent>("HealthComponent");
		
	PlayerInventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("PlayerInventory"));
	PlayerInventory->SetSlotsCapacity(20);
	PlayerInventory->SetWeightCapacity(50.0f);
	
	WeaponComponent = CreateDefaultSubobject<UWeaponsComponent>(TEXT("WeaponComponent"));
	InteractionCheckFrequency = 0.1;

	InteractionCheckDistance = 200.0f;

	BaseEyeHeight = 100.0f;
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)
}


void ASHGameCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	check (CharHealth);
	OnHealthChange(CharHealth->GetHealth());
	CharHealth->OnDeath.AddUObject(this, &ASHGameCharacter::OnDead);
	CharHealth->OnHealthChange.AddUObject(this, &ASHGameCharacter::OnHealthChange);
	HUD = Cast<AMyHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());

	FOnTimelineFloat AimLerpAlphaValue;
	FOnTimelineEvent TimelineFinishedEvent;
	AimLerpAlphaValue.BindUFunction(this,FName("UpdateCameraTimeLine"));
	TimelineFinishedEvent.BindUFunction(this,FName("CameraTimelineEnd"));

	if(AimingCameraTimeLine && AimingCameraCurve)
	{
		AimingCameraTimeLine->AddInterpFloat(AimingCameraCurve,AimLerpAlphaValue);
		AimingCameraTimeLine->SetTimelineFinishedFunc(TimelineFinishedEvent);
	}
	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void ASHGameCharacter::Tick(float DeltaSeconds)
{
	
	Super::Tick(DeltaSeconds);
	
	if(GetWorld()->TimeSince(InteractionData.LastInteractionCheckTime)>InteractionCheckFrequency)
	{
		PerformInteractionCheck();
	}

	
	if(SprintState==true && Stamina !=0.0f)
	{
		DecriseStamina();
	}
	else
	{
		if(bPressedJump==true || SprintState == false && Stamina!=100.f)
		{
			IncriseStamina();
		}
	}
}


//////////////////////////////////////////////////////////////////////////
// Input

void ASHGameCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		check(EnhancedInputComponent);
		check(WeaponComponent);
		
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);
		//Crouch
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Started, this, &ASHGameCharacter::InputCrouchPressed);
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Completed, this, &ASHGameCharacter::InputCrouchReleased);
		
		EnhancedInputComponent->BindAction(TogglMenu, ETriggerEvent::Triggered, this, &ASHGameCharacter::ToggleMenu);
		//Aiming
		EnhancedInputComponent->BindAction(AimAction,ETriggerEvent::Started,this,&ASHGameCharacter::InputAimPressed);
		EnhancedInputComponent->BindAction(AimAction,ETriggerEvent::Completed,this,&ASHGameCharacter::InputAimReleased);
		//Interaction

		EnhancedInputComponent->BindAction(Interacted,ETriggerEvent::Started,this,&ASHGameCharacter::BeginInteract);
		EnhancedInputComponent->BindAction(Interacted,ETriggerEvent::Completed,this,&ASHGameCharacter::EndInteract);
		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ASHGameCharacter::Move);
		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Started, this, &ASHGameCharacter::InputWalkPressed);
		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Completed, this, &ASHGameCharacter::InputWalkReleased);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &ASHGameCharacter::InputSprintPressed);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &ASHGameCharacter::InputSprintReleased);
		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ASHGameCharacter::Look);
		//Fire
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Started,WeaponComponent, &UWeaponsComponent::FireStart);
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Completed,WeaponComponent, &UWeaponsComponent::FireEnd);
		// NextWeapon
		EnhancedInputComponent->BindAction(NextWeaponAction, ETriggerEvent::Started,WeaponComponent, &UWeaponsComponent::NextWeapon);
		EnhancedInputComponent->BindAction(ReloadWeaponAction, ETriggerEvent::Started,WeaponComponent, &UWeaponsComponent::Reload);
		
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void ASHGameCharacter::InputWalkPressed()
{
	WalkState = true;
	ChangeMovementState();
}

void ASHGameCharacter::InputWalkReleased()
{
	WalkState = false;
	ChangeMovementState();
}


void ASHGameCharacter::InputSprintPressed()
{
	SprintState = true;
	ChangeMovementState();
	DecriseStamina();
	
}

void ASHGameCharacter::InputSprintReleased()
{
	
	SprintState = false;
	ChangeMovementState();
	IncriseStamina();
}

void ASHGameCharacter::InputAimPressed()
{
	AimState = true;
	ChangeMovementState();
	
}

void ASHGameCharacter::InputAimReleased()
{
	AimState = false;
	ChangeMovementState();
	
}

void ASHGameCharacter::InputCrouchPressed()
{
	CrouchState = true;
	ChangeMovementState();
	Crouch();
	
}

void ASHGameCharacter::InputCrouchReleased()
{
	CrouchState = false;
	ChangeMovementState();
	UnCrouch();
}

void ASHGameCharacter::PerformInteractionCheck()
{
	InteractionData.LastInteractionCheckTime = GetWorld()->GetTimeSeconds();

	//
	
	FVector TraceStart{GetPawnViewLocation()};

	//
	FVector TraceEnd{TraceStart + (GetViewRotation().Vector() * InteractionCheckDistance)};

	float LookDiraction = FVector::DotProduct(GetActorForwardVector(),GetViewRotation().Vector());
	
	if(LookDiraction>0)
	{
		DrawDebugLine(GetWorld(),TraceStart,TraceEnd,FColor::Red,false,1.0f,0,2.0f);
	
		FCollisionQueryParams QueryParams;

		QueryParams.AddIgnoredActor(this);

		FHitResult TraceHit;

		if(GetWorld()->LineTraceSingleByChannel(TraceHit,TraceStart,TraceEnd,ECC_Visibility,QueryParams))
		{

			if(TraceHit.GetActor()->GetClass()->ImplementsInterface(UInteractionInterface::StaticClass()))
			{
				if(TraceHit.GetActor() != InteractionData.CurrentInteractable)
				{
					FoundInteractable((TraceHit.GetActor()));
					return;
				}

				//Если верхняя часть не удалась,то возвращаем кюрент
				if(TraceHit.GetActor() == InteractionData.CurrentInteractable)
				{
					return;
				}
			}
		}
	}
	NoInteractableFound();
}

void ASHGameCharacter::FoundInteractable(AActor* NewInteractable)
{
	if(IsInteracting())
	{
		EndInteract();
	}

	if(InteractionData.CurrentInteractable)
	{
		TargetInteractable = InteractionData.CurrentInteractable;
		TargetInteractable->EndFocus();
	}

	InteractionData.CurrentInteractable = NewInteractable;
	TargetInteractable = NewInteractable;

	//Обновление данных виджета взаимодействия
	HUD->UdpateInteractionWidget(&TargetInteractable->InteractableData);
	
	TargetInteractable->BeginFocus();
}

void ASHGameCharacter::NoInteractableFound()
{
	if(IsInteracting())
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_Interaction);
	}

	if(InteractionData.CurrentInteractable)
	{
		if(IsValid(TargetInteractable.GetObject()))
		{
			TargetInteractable->EndFocus();
		}

		//спрятать виджкт взаимодействия HUD
		HUD->HideInteractionWidget();
		
		InteractionData.CurrentInteractable = nullptr;
		TargetInteractable = nullptr;
	}
}

void ASHGameCharacter::BeginInteract()
{
	//проверка ничего не изменилось с взаимодействием
	PerformInteractionCheck();

	if(InteractionData.CurrentInteractable)
	{
		if(IsValid((TargetInteractable.GetObject())))
		{
			TargetInteractable->BeginInteract();

			if(FMath::IsNearlyZero(TargetInteractable->InteractableData.InteractionDuration,0.1f))
			{
				Interact();
			}
			else
			{
				GetWorldTimerManager().SetTimer(TimerHandle_Interaction,
					this,
					&ASHGameCharacter::Interact,
					TargetInteractable->InteractableData.InteractionDuration,
					false);
			}
			
		}
	}
}

void ASHGameCharacter::EndInteract()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_Interaction);

	if(IsValid((TargetInteractable.GetObject())))
		{
			TargetInteractable->EndInteract();
		}
	
}

void ASHGameCharacter::Interact()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_Interaction);

	if(IsValid((TargetInteractable.GetObject())))
	{
		TargetInteractable->Interact(this);
	}
}

void ASHGameCharacter::UpdateInteractionWidget() const
{
	
	if(IsValid((TargetInteractable.GetObject())))
	{
		//обновление информаци взаимодействия в нашем виджите
		HUD->UdpateInteractionWidget(&TargetInteractable->InteractableData);
	}
}

void ASHGameCharacter::ToggleMenu()
{
	HUD->ToggleMenu();
}

// void ASHGameCharacter::CanFire()
// {
// 	
// }

void ASHGameCharacter::Aim()
{
	if(!HUD->bIsMenuVisible)
	{
		bAiming = true;
		bUseControllerRotationYaw = true;
		GetCharacterMovement()->MaxWalkSpeed = 100.0f;

		if(AimingCameraTimeLine)
			AimingCameraTimeLine->PlayFromStart();
	}
}

void ASHGameCharacter::StopAiming()
{
	if(bAiming)
	{
		bAiming = false;
		bUseControllerRotationYaw = false;
		GetCharacterMovement()->MaxWalkSpeed = 500.0f;

		if(AimingCameraTimeLine)
			AimingCameraTimeLine->Reverse();
	}
}

void ASHGameCharacter::CharacterUpdateSpeed()
{
	float ResSpeed = 400.0f;
	switch (MovementState) {
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	case EMovementState::Crouch_State:
		ResSpeed = MovementSpeedInfo.CrouchSpeedNormal;
		break;
	case EMovementState::CrouchAim_State:
		ResSpeed = MovementSpeedInfo.CrouchSpeedNormal;
		break;
	case EMovementState::CrouchRun_State:
		ResSpeed=MovementSpeedInfo.CrouchRunSpeedRun;
		break;
	default: ;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ASHGameCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_State;
	if(!WalkState && !AimState && !SprintState && !CrouchState)
	{
		 NewState = EMovementState::Run_State;
	}
	else
	{
		if(WalkState && !AimState && !SprintState && !CrouchState)
		{
			NewState = EMovementState::Walk_State;
		}
		else
		{
			if(!WalkState && AimState && !SprintState)
			{
				NewState = EMovementState::Aim_State;
			}
			else
			{
				if(SprintState)
				{
					CrouchState = false;
					WalkState = false;
					AimState = false;
					NewState = EMovementState::SprintRun_State;
				}
				else
				{
					if(WalkState && AimState && !SprintState && !CrouchState)
					{
						NewState = EMovementState::AimWalk_State;
					}
					else
					{
						if(CrouchState && !WalkState && !AimState)
						{
							SprintState = false;
							NewState = EMovementState::Crouch_State;
						}
						else
						{
							if(CrouchState && !WalkState && AimState && !SprintState)
							{
								NewState = EMovementState::Crouch_State;
							}
							else
							{
								if(CrouchState && !WalkState && !AimState && SprintState )
								{									
									NewState = EMovementState::CrouchRun_State;
								}
							}
						}
					}
				}
			}
			
		}
	}
	SetMovementState(NewState);
}

EMovementState ASHGameCharacter::GetMovementState()
{
	return MovementState;
}

void ASHGameCharacter::SetMovementState(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdateSpeed();
}



void ASHGameCharacter::DecriseStamina()
{
		CurrentStamina = Stamina - DecStamina;
		Stamina = CurrentStamina;
		//GEngine->AddOnScreenDebugMessage(-1,5.f,FColor::Red,FString::Printf(TEXT("Stamina Decrise %f"),Stamina));
		
		if(CurrentStamina <=0)
		{
			Stamina = 0;
			InputSprintReleased();
		}
	
	
}

void ASHGameCharacter::IncriseStamina()
{
	
		CurrentStamina = Stamina + IncStamina;
		Stamina = CurrentStamina;
		//GEngine->AddOnScreenDebugMessage(-1,5.f,FColor::Green,FString::Printf(TEXT("Stamina Incrise %f") ,Stamina));
	
		if(CurrentStamina >=100)
		{
			Stamina = 100;
		}
	
	
}

float ASHGameCharacter::GetCurrentStamina(float NewStamina)
{
	return Stamina;
}

bool ASHGameCharacter::GetIsAlive()
{
	return bIsAlive;
}

void ASHGameCharacter::UpdateCameraTimeLine(const float TimeLineValue) const
{
	const FVector CameraLocation = FMath::Lerp(DefaultCameraLocation,AimingCameraLocation,TimeLineValue);
	CameraBoom->SocketOffset = CameraLocation;
}

void ASHGameCharacter::CameraTimeLineEnd()
{
	if(AimingCameraTimeLine)
	{
		if(AimingCameraTimeLine->GetPlaybackPosition() != 0.0f)
		{
			//HUD->DisplayCrosshair();
		}
	}
}

void ASHGameCharacter::DropItem(UItemBase* ItemToDrop, const int32 QuantityToDrop)
{
	if(PlayerInventory->FindMathingItem(ItemToDrop))
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.bNoFail = true;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

		const FVector SpawnLocation{GetActorLocation() + (GetActorForwardVector() * 50.0f)};

		const FTransform SpawnTransform(GetActorRotation(),SpawnLocation);

		const int32 RemovedQuantity = PlayerInventory->RemoveAmountOfItem(ItemToDrop,QuantityToDrop);

		APickUp* Pickup = GetWorld()->SpawnActor<APickUp>(APickUp::StaticClass(),SpawnTransform,SpawnParams);

		Pickup->InitializeDrop(ItemToDrop,RemovedQuantity);
		
	}
	else
	{
		{
			UE_LOG(LogTemp,Warning,TEXT("Item to drop was somehow null!"));
		}
	}
}



void ASHGameCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ASHGameCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}




float ASHGameCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                   AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		//CharHealth->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			//UTypes::AddEffectBySurfaceType(this,NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfuceType());//to do Name None-bone for radiodamage
		}
		AProjectile_Grenade* MyGrenade = Cast<AProjectile_Grenade>(DamageCauser);
		if(MyGrenade)
		{
			
		}
	}

	return ActualDamage;
}

void ASHGameCharacter::OnDead()
{
	PlayAnimMontage(DeathAnimMontage);
	GetCharacterMovement()->DisableMovement();
	UE_LOG(LogTemplateCharacter,Display,TEXT("Player % is Dead"), *GetName());
}

void ASHGameCharacter::OnHealthChange(float Health)
{
	
}
