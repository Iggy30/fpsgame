#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "Logging/LogMacros.h"
#include "InputAction.h"
#include "SHGame/Data/ItemDataStructs.h"
#include "Components/InventoryComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Interface/InteractionInterface.h"
#include "SHGameCharacter.generated.h"


class UTextRenderComponent;
class UCharacterHealthComponent;
class UWeaponsComponent;
class UTimelineComponent;
class AMyHUD;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

USTRUCT()
struct FInteractionData 
{
	GENERATED_BODY()
	
	FInteractionData() : CurrentInteractable(nullptr),LastInteractionCheckTime(0.0f)
	{
		
	};
	
	UPROPERTY()
	AActor* CurrentInteractable;

	UPROPERTY()
	float LastInteractionCheckTime;
};


UCLASS(config=Game)
class ASHGameCharacter : public ACharacter
{
	GENERATED_BODY()
	
public:
	//========================================================================
	//PROPERTIES & VARIABLES
	//=========================================================================

	bool bAiming;
	ASHGameCharacter();
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
		UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
		UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
	UInputAction* TogglMenu;
	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
		UInputAction* MoveAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
	UInputAction* RunAction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
	UInputAction* WalkAction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
	UInputAction* CrouchAction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
	UInputAction* SprintAction;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
	UInputAction* AimAction;
	
	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnchancedInput")
		UInputAction* LookAction;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "EnchancedInput")
		UInputAction* Interacted;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "EnchancedInput")
	UInputAction* FireAction;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "EnchancedInput")
	UInputAction* NextWeaponAction;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "EnchancedInput")
	UInputAction* ReloadWeaponAction;
	
	FORCEINLINE bool IsInteracting() const {return GetWorldTimerManager().IsTimerActive(TimerHandle_Interaction); };

	FORCEINLINE UInventoryComponent* GetInventory() const {return PlayerInventory;};

	void UpdateInteractionWidget() const;

	void DropItem(UItemBase* ItemToDrop,const int32 QuantityToDrop);

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Components")
	UWeaponsComponent* WeaponComponent;

	// Movements
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementSpeedInfo;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState();
protected:
	//========================================================================
	//PROPERTIES & VARIABLES
	//=========================================================================

	UPROPERTY()
	AMyHUD* HUD;


	/** Follow camera */
	UPROPERTY(EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FPSCamera;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "Components")
	UCharacterHealthComponent* CharHealth;

	UPROPERTY(VisibleAnywhere,Category = "Character | Interaction")
	TScriptInterface<IInteractionInterface> TargetInteractable;

	UPROPERTY(VisibleAnywhere,Category = " Character | Inventory")
	UInventoryComponent* PlayerInventory;
	
	//Частота проверки взаимодействия
	float InteractionCheckFrequency;

	float InteractionCheckDistance;
	//Таймер для взаимодействия
	FTimerHandle TimerHandle_Interaction;
	
	FInteractionData InteractionData;

	// timeline properties used for camera aiming transition
	UPROPERTY(VisibleAnywhere,Category = "Character | Camera")
	FVector DefaultCameraLocation;
	UPROPERTY(VisibleAnywhere,Category = "Character | Camera")
	FVector AimingCameraLocation;
	
	TObjectPtr<UTimelineComponent> AimingCameraTimeLine;

	UPROPERTY(EditDefaultsOnly,Category= "Character | Aim TimeLine")
	UCurveFloat* AimingCameraCurve;

	//Inputs==========================================================
	void InputWalkPressed();
	void InputWalkReleased();
	void InputSprintPressed();
	void InputSprintReleased();
	void InputAimPressed();
	void InputAimReleased();
	void InputCrouchPressed();
	void InputCrouchReleased();
	
	//========================================================================
	//FUNCTIONS
	//=========================================================================

	void PerformInteractionCheck();

	void FoundInteractable(AActor* NewInteractable);

	void NoInteractableFound();

	void BeginInteract();

	void EndInteract();

	void Interact();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();
	virtual void Tick(float DeltaSeconds) override;

	void ToggleMenu();

	//void CanFire();
	void Aim();
	void StopAiming();

		
	// Movement============
	bool WalkState = false;
	bool AimState = false;
	bool SprintState = false;
	bool CrouchState = false;
	
	UFUNCTION(BlueprintCallable)
	void CharacterUpdateSpeed();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	
	UPROPERTY()
	EMovementState MovementState = EMovementState::Run_State;
	
	//Stamina===========================
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Stamina",meta =(ClampMin = 0.0f,ClampMax=100.0f))
	float Stamina = 100.0f;

	float CurrentStamina;
	float DecStamina = 0.1f;
	float IncStamina = 0.07f;
	
	UFUNCTION(BlueprintCallable, Category = "Stamina")
	void DecriseStamina();
	UFUNCTION(BlueprintCallable, Category = "Stamina")
	void IncriseStamina();

	float GetCurrentStamina(float NewStamina);
	
	// Alive or Dead
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive();
	bool bIsAlive = true;

	
	UFUNCTION()
	void UpdateCameraTimeLine(const float TimeLineValue) const;
	UFUNCTION()
	void CameraTimeLineEnd();

	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	//void Jump() override;
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;
	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Sprint)
	// USoundCue* NewCue;
	UPROPERTY(EditDefaultsOnly,Category = "AnimationDeath")
	UAnimMontage* DeathAnimMontage;

	UFUNCTION()
	void SetMovementState(EMovementState NewState);
private:
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	void OnDead();
	void OnHealthChange(float Health);
};
