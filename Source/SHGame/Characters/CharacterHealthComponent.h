#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterHealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChange, float);
DECLARE_MULTICAST_DELEGATE(FOnDeath);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHGAME_API UCharacterHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterHealthComponent();

	//UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnHealthChange OnHealthChange;
	//UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDeath OnDeath;
	UFUNCTION(BlueprintCallable)
	bool IsDead() const {return FMath::IsNearlyZero(Health);};
	
	UFUNCTION(BlueprintCallable,Category = "Health")
	float GetHealthPercent() const { return Health / MaxHealth;}
	UFUNCTION(BlueprintCallable,Category = "Health")
	float GetHealth() const {return Health;}

	
		
protected:

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Health",meta =(ClampMin = 0.0f,ClampMax=100.0f))
	float MaxHealth = 100.0f;
	// Called when the game starts
	virtual void BeginPlay() override;
	

private:
	
	float Health = 0.0f;
	UFUNCTION()
	void OnTakeAnyDamageHandle( AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser );
	void SetHealth(float NewHealth);
};
