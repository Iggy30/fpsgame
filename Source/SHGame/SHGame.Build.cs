// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SHGame : ModuleRules
{
	public SHGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] {  "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput" ,
            "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });

        PrivateDependencyModuleNames.AddRange(new string[]
            {  "OnlineSubsystem","OnlineSubsystemNull","OnlineSubsystemUtils"});
    }
}
