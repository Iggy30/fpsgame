// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractionInterface.generated.h"

class ASHGameCharacter;

//Взаимодействующий перечень обьектов
UENUM()
enum class EInteractableType : uint8
{
	Pickup UMETA(DisplayName = "Pickup"),
	NonPlayerCharacter UMETA(DisplayName = "NonPlayerCharacter"),
	Device UMETA(DisplayName = "Device"),
	Toggle UMETA(DisplayName = "Toggle"),
	Container UMETA(DisplayName = "Container")
};

USTRUCT()
struct FInteractableData
{
	GENERATED_BODY()

	FInteractableData() :
	InteractableType(EInteractableType::Pickup),
	Name(FText::GetEmpty()),
	Action(FText::GetEmpty()),
	Quantity(0),
	InteractionDuration(0.f)
	{
		
	};
	
	UPROPERTY(EditInstanceOnly)
	EInteractableType InteractableType;
	
	UPROPERTY(EditInstanceOnly)
	FText Name;
	
	UPROPERTY(EditInstanceOnly)
	FText Action;

	//Используется только для pickup
	UPROPERTY(EditInstanceOnly)
	int8 Quantity;

	//Время взаимодействия с дверями,обьектами и т.д.
	UPROPERTY(EditInstanceOnly)
	float InteractionDuration;
};
// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractionInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHGAME_API IInteractionInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//Начало действия зажатия клавишы
	virtual void BeginFocus();
	//Конец действия зажатия клавиши
	virtual void EndFocus();
	//Начало взаимодействия
	virtual  void BeginInteract();
	//Конец взаимодействия
	virtual void EndInteract();
	//Взаимодействия(если Интеракт = 0 то вызываем Interact на прямую)
	virtual void Interact(ASHGameCharacter* PlayerCharacter);

	//Структура
	FInteractableData InteractableData;
};
