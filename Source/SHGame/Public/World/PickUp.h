// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/InteractionInterface.h"
#include "SHGame/Characters/SHGameCharacter.h"
#include "PickUp.generated.h"

class UDataTable;
class UItemBase;


UCLASS()
class SHGAME_API APickUp : public AActor, public  IInteractionInterface
{
	GENERATED_BODY()
	
public:
	
	// Sets default values for this actor's properties
	APickUp();

	void InitializePickup(const int32 InQuantity);

	void InitializeDrop(UItemBase* ItemDrop,const int32 InQuantity);

	FORCEINLINE UItemBase* GetItemData() {return ItemReference;};

	virtual void BeginFocus() override;
	virtual void EndFocus() override;
	
protected:

	UPROPERTY(VisibleAnywhere,Category = "Pickup | Components")
	UStaticMeshComponent* PickupMesh;

	//таблица данных
	UPROPERTY(EditInstanceOnly,Category = "Pickup | Item Initialization")
	UDataTable* ItemDataTable;

	UPROPERTY(EditInstanceOnly,Category = "Pickup | Item Initialization")
	FName DesiredItemID;

	UPROPERTY(VisibleAnywhere,Category = "Pickup | Item Reference")
	UItemBase* ItemReference;

	UPROPERTY(EditInstanceOnly,Category = "Pickup | Item Initialization")
	int32 ItemQuantity;

	UPROPERTY(VisibleInstanceOnly,Category = "Pickup | Interaction")
	FInteractableData InstanceInteractableData;

	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void Interact(ASHGameCharacter* PlayerCharacter) override;
	//обновление интерактивных двнных,@внутренняя функция @
	void UpdateInteractableData();
	
	void TakePickup(const ASHGameCharacter* Taker);

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
	
};
