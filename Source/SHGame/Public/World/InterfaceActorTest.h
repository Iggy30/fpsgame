// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/InteractionInterface.h"
#include "InterfaceActorTest.generated.h"

UCLASS()
class SHGAME_API AInterfaceActorTest : public AActor,public IInteractionInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInterfaceActorTest();

protected:
	UPROPERTY(EditAnywhere,Category = "Test Actor")
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere,Category = "Test Actor")
	FInteractableData InstanceInteractableData;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	virtual void BeginFocus() override;
	virtual void EndFocus() override;
	virtual void BeginInteract() override;
	virtual void EndInteract() override;
	virtual void Interact(ASHGameCharacter* PlayerCharacter) override;
};
