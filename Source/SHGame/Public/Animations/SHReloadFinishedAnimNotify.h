// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animations/SHAnimNotify.h"
#include "SHReloadFinishedAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class SHGAME_API USHReloadFinishedAnimNotify : public USHAnimNotify
{
	GENERATED_BODY()
	
};
