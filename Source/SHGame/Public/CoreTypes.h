﻿#pragma once

#include "CoreTypes.generated.h"

class AWeaponDefault;

//Weapon
DECLARE_MULTICAST_DELEGATE(FOnClipEmptySignature);

USTRUCT(BlueprintType)
struct FAmmoData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	int32 Bullets;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	int32 Clips;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	bool Infinite;
	
};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	TSubclassOf<AWeaponDefault> WeaponClass;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	UAnimMontage* FireAnimMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	UAnimMontage* ReloadAnimMontage;
	
};
