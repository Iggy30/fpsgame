// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SHGame/Characters/SHGameCharacter.h"
#include "SHGame/Data/ItemDataStructs.h"
#include "ItemBase.generated.h"

class UInventoryComponent;

UCLASS()
class SHGAME_API UItemBase : public UObject
{
	GENERATED_BODY()
	
public:
//========================================================================
//PROPERTIES & VARIABLES
//=========================================================================
	UPROPERTY()
	UInventoryComponent* OwningInventory;
	
	//Количестко
	UPROPERTY(VisibleAnywhere,Category= "Item")
	int32 Quantity;
	
	UPROPERTY(VisibleAnywhere, Category = "Item")
	FName ID;

	UPROPERTY(VisibleAnywhere,Category= "Item")
	EItemType ItemType;

	// Weapon Wear Slot
	UPROPERTY(VisibleAnywhere,Category= "Item")
	EWeaponType WeaponType;	// Armor Wear Slot
	// Armor Wear Slot
	UPROPERTY(VisibleAnywhere,Category= "Item")
	EArmorType ArmorType;
	
	
	UPROPERTY(VisibleAnywhere, Category = "Item")
	EItemQuality ItemQuality;

	UPROPERTY(VisibleAnywhere, Category = "Item")
	FItemStatistics ItemStatistics;

	UPROPERTY(VisibleAnywhere, Category = "Item")
	FItemTextData TextData;

	UPROPERTY(VisibleAnywhere, Category = "Item")
	FItemNumericData NumericData;

	UPROPERTY(EditAnywhere, Category = "Item")
	FItemAssetData AssetData;

	bool bIsCopy;
	bool bIsPickup;

	//========================================================================
	//FUNCTIONS
	//=========================================================================
	UItemBase();

	void ResetItemFlags();
	
	UFUNCTION(Category = "Item")
	UItemBase* CreateItemCopy();
	
	UFUNCTION(Category = "Item")
	FORCEINLINE float GetItemsStackWeight() const { return Quantity * NumericData.Weight;};
	
	//Один вес предмета(сам вес)
	UFUNCTION(Category = "Item")
	FORCEINLINE float GetItemSingleWeight() const {return NumericData.Weight;};
	
	//Полный стек предметов или соответсвует ли макс количеству размер стека,если значение tru то значит поолный стек
	UFUNCTION(Category = "Item")
	FORCEINLINE bool IsFullItemStack() const {return Quantity == NumericData.MaxStakSize;};

	//Принимает новое число
	UFUNCTION(Category = "Item")
	void SetQuantity(const int32 NewQuantity);
	
	//Функция ссылается на Игрока
	UFUNCTION(Category = "Item")
	virtual void Use(ASHGameCharacter* Character);
protected:

	bool operator==(const FName& OtherID ) const
	{
		return this->ID == OtherID;
	}
};
