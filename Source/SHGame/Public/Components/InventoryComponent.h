// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

//обьявление дилегата многоадресной рассылки 
DECLARE_MULTICAST_DELEGATE(FInventoryUpdate);


class UItemBase;


UENUM(BlueprintType)
enum class EItemAddResult : uint8
{
	IAR_NoItemAdded UMETA(DisplayName = "No item added"),
	IAR_PartialAmountItemAdded UMETA(DisplayName = "Partial amount of item added"),
	IAR_AllItemAdded UMETA(DisplayName = "All item added")
	
};

USTRUCT(BlueprintType)
struct FItemAddResult
{
	GENERATED_BODY()

	FItemAddResult() :
	ActualAmountAdded(0),
	OperationResult(EItemAddResult::IAR_NoItemAdded),
	ResultMessage(FText::GetEmpty())
	{};

	//Actual amount item that was added to the inventory	
	UPROPERTY(BlueprintReadOnly,Category = "Item Add Result")
	int32 ActualAmountAdded;
	//Enum representing the end state of an add item operation
	UPROPERTY(BlueprintReadOnly,Category = "Item Add Result")
	EItemAddResult OperationResult;
	//Informational message that can be passed with the result
	UPROPERTY(BlueprintReadOnly,Category = "Item Add Result")
	FText ResultMessage;

	//вызов функции на прямую(вспомогательные функции,которые будут возвращать результат)
	static FItemAddResult AddedNone(const FText& ErrorText)
	{
		//создаем новую функцию и что фактическая сумма равна 0 и наш товар не добавляется
		FItemAddResult AddedNoneResult;
		AddedNoneResult.ActualAmountAdded = 0;
		AddedNoneResult.OperationResult = EItemAddResult::IAR_NoItemAdded;
		AddedNoneResult.ResultMessage = ErrorText;
		//возвращаем добавленный не разрешенный элемент
		return AddedNoneResult;
	};
	static FItemAddResult AddedPartial(const int32 PartialAmountAdded,const FText& ErrorText)
	{
	
		FItemAddResult AddedPartialResult;
		AddedPartialResult.ActualAmountAdded = PartialAmountAdded;
		AddedPartialResult.OperationResult = EItemAddResult::IAR_PartialAmountItemAdded;
		AddedPartialResult.ResultMessage = ErrorText;
	
		return AddedPartialResult;
	}
	static FItemAddResult AddedAll(const int32 AmountAdded ,const FText& Message)
	{

		
		FItemAddResult AddedAllResult;
		AddedAllResult.ActualAmountAdded = AmountAdded;
		AddedAllResult.OperationResult = EItemAddResult::IAR_AllItemAdded;
		AddedAllResult.ResultMessage = Message;
	
		return AddedAllResult;
	}

	
};

//надо продумать как будут складываться вещи в инвенетарь

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHGAME_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	//========================================================================
	//PROPERTIES & VARIABLES
	//=========================================================================

	//Создание экземпляра делегата
	FInventoryUpdate OnInventoryUpdated;
	
	
	//========================================================================
	//FUNCTIONS
	//=========================================================================
	UInventoryComponent();

	//основная функция,то что мы пытаемся добавить в инвентарь
	UFUNCTION(Category = "Inventory")
	FItemAddResult HandleAddItem(UItemBase* InputItem);
	UFUNCTION(Category = "Inventory")
	UItemBase* FindMathingItem(UItemBase* ItemIn) const;
	//поиск след предмета по ID
	UFUNCTION(Category = "Inventory")
	UItemBase* FindNextItemID(UItemBase* ItemIn) const;
	//поиск след частичтного стека
	UFUNCTION(Category = "Inventory")
	UItemBase* FindNextParticalStack(UItemBase* ItemIn) const;
	
	//функция удаления одного экземпляра предмета полностью из инвентарного массива
	UFUNCTION(Category = "Inventory")
	void RemoveSingleInstanceOfItem(UItemBase* ItemIn);
	//удаление желаемой суммы для удаленрия
	UFUNCTION(Category = "Inventory")
	int32 RemoveAmountOfItem(UItemBase* ItemToRemove,int32 DesiredAmountToRemove);
	//разделение существующего стэка на две части
	UFUNCTION(Category = "Inventory")
	void SplitExistingStack(UItemBase* ItemIn,const int32 AmountTipSplit);

	//--------------
	//Getters
	//---------------
	//получить общий вес мнвентаря
	UFUNCTION(Category = "Inventory")
	FORCEINLINE float GetInventoryTotalWeight() const {return InventoryTotalWeight;};
	//Получить вес вместимости
	UFUNCTION(Category = "Inventory")
	FORCEINLINE float GetWeightCapacity() const{return InventoryWeightCapacity;};
	//Получить слоты вместимости
	UFUNCTION(Category = "Inventory")
	FORCEINLINE int32 GetSlotsCapacity() const {return InventorySlotsCapacity;};
	//Создание ссылки на массив инвентаря
	UFUNCTION(Category = "Inventory")
	FORCEINLINE TArray<UItemBase*> GetInventoryContents() const {return InventoryContents;};

	//--------------
	//Setters
	//---------------

	//задать уоличество слотов вместимости
	UFUNCTION(Category = "Inventory")
	FORCEINLINE void SetSlotsCapacity(const int32 NewSlotsCapacity) { InventorySlotsCapacity = NewSlotsCapacity;};
	//Задать вес вместимости(предметов)
	UFUNCTION(Category = "Inventory")
	FORCEINLINE void SetWeightCapacity(const float NewWeightCapacity) {InventoryWeightCapacity = NewWeightCapacity;};
protected:
	//========================================================================
	//PROPERTIES & VARIABLES
	//=========================================================================
	UPROPERTY(VisibleAnywhere,Category = "Inventory")
	float InventoryTotalWeight;
	//Слоты инвентаря вместимость
	UPROPERTY(EditInstanceOnly,Category = "Inventory")
	int32 InventorySlotsCapacity;
	
	UPROPERTY(EditInstanceOnly,Category = "Inventory")
	float InventoryWeightCapacity;
	
	//(указателть на оьтект Т) База наших предметов,фактически это наш инвентарь.
	UPROPERTY(VisibleAnywhere,Category = "Inventory")
	TArray<TObjectPtr<UItemBase>> InventoryContents;


	
	//========================================================================
	//FUNCTIONS
	//=========================================================================
	virtual void BeginPlay() override;

	//Обработка не "стыкуемые элементы" которые входят в базу U элеменьов
	FItemAddResult HandleNonStackableItems(UItemBase* InputItem);
	//обработка элементов 
	int32 HandleStackableItems(UItemBase* ItemIn,int32 RequestedAddAmount);
	//Расчет веса и добавление сумм
	int32 CalculateWeightAddAmount(UItemBase* ItemIn,int32 RequestedAddAmount);
	//Вычисление числа для полного стека
	int32 CalculateNumberForFullStack(UItemBase* StackableItem,int32 InitialRequestedAddAmount);


	void AddNewItem(UItemBase* Item,const int32 AmountToAdd);
};
