// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SHGame/Weapons/WeaponDefault.h"
#include "SHGame/Weapons/Projectile/ProjectileDefault.h"
#include "SHRiffleWeapon.generated.h"

/**
 * 
 */
UCLASS()
class SHGAME_API ASHRiffleWeapon : public AWeaponDefault
{
	GENERATED_BODY()

public:
	virtual void FireStart() override;
	virtual void FireStop()override;

	
protected:

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	float TimeBetweenShots = 0.1f;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	float BulletSpread = 0.5f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
	float DamageAmount = 10.0f;

	virtual void MakeShot() override;
	virtual bool GetTraceData(FVector& TraceStart,FVector& TraceEnd) const override;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	TSubclassOf<AProjectileDefault> ProgectileClass;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	USoundBase* FireSound;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	USoundBase* AmmoSound;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	UParticleSystem* FXFire;
private:
	FTimerHandle ShotTimerHandle;
	
		
	void MakeDamage(const FHitResult& HitResult);
};
