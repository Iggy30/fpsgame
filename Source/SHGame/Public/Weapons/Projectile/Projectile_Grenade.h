// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile_Grenade.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UCLASS()
class SHGAME_API AProjectile_Grenade : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile_Grenade();

	void SetShotDiraction(const FVector& Diraction)
	{
		ShotDiraction= Diraction;
	}
	
protected:
	UPROPERTY(VisibleAnywhere,Category = "Weapon")
	USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere,Category = "Weapon")
	UProjectileMovementComponent* MovementComponent;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	float DamageRadius = 200.0f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	float DamageAmout = 50.0f;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	bool DoFullDamage = false;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	float LifeSeconds = 5.0f;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Projectile")
	UParticleSystem* ExploseFX;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Projectile")
	USoundBase* ExploseSound;

	//UPROPERTY()
	//TSubclassOf<UDamageType> DamageType;
	//float UDamageType = DamageAmout;
private:
	
	FVector ShotDiraction;

	UFUNCTION()
	void OnProjectileHit( UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit );
	
	AController* GetController() const;
public:
	void FXExplose(UParticleSystem* FX,USoundBase* ExploseS);
};
