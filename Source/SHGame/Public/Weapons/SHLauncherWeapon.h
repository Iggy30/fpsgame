// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SHGame/Weapons/WeaponDefault.h"
#include "SHLauncherWeapon.generated.h"

class AProjectile_Grenade;
/**
 * 
 */
UCLASS()
class SHGAME_API ASHLauncherWeapon : public AWeaponDefault
{
	GENERATED_BODY()

	
public:
	virtual void FireStart() override;


protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon")
	TSubclassOf<AProjectile_Grenade> ProgectileClass;

	virtual void MakeShot() override;
};
