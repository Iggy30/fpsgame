// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/WrapBox.h"
#include "InventoryPanel.generated.h"

class UTextBlock;
class UInventoryItemSlot;
class UInventoryComponent;
class ASHGameCharacter;
/**
 * 
 */
UCLASS()
class SHGAME_API UInventoryPanel : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void RefresInventory();

	UPROPERTY(meta=(BindWidget))
	UWrapBox* InventoryWrapBox;
	
	UPROPERTY(meta=(BindWidget))
	UTextBlock* WeighInfo;
	
	UPROPERTY(meta=(BindWidget))
	UTextBlock* CapacityInfo;

	//Обьявление ссылки на игрока
	UPROPERTY()
	ASHGameCharacter* PlayerCharacter;
	
	//Ссылка от  персонажа на  инвентарь
	UPROPERTY()
	UInventoryComponent* InventoryReference;
	
	//Подкласс Т который является слотом элемента инвентаря
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UInventoryItemSlot> InventorySlotClass;

protected:
	void SetInfoText() const;
	virtual void NativeOnInitialized() override;
	virtual bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent,
		UDragDropOperation* InOperation) override;
};
