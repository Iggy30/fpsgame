// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MainMenu.h"
#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

struct FInteractableData;
class UInteractionWidget;
class UMainMenu;

UCLASS()
class SHGAME_API AMyHUD : public AHUD
{
	GENERATED_BODY()
public:
	//========================================================================
	//PROPERTIES & VARIABLES
	//=========================================================================
	
	//Создание пары подкласс пользователя системы Т(подкласс главного меню)
	UPROPERTY(EditDefaultsOnly,Category = "Widgets")
	TSubclassOf<UMainMenu> MainMenuClass;

	UPROPERTY(EditDefaultsOnly,Category = "Widgets")
	TSubclassOf<UInteractionWidget> InteractionWidgetClass;

	bool bIsMenuVisible;

	//========================================================================
	//FUNCTIONS
	//=========================================================================
	AMyHUD();

	void DisplayMenu();
	void HideMenu();
	void ToggleMenu();
	
	void ShowInteractionWidget() const;
	void HideInteractionWidget() const;

	void UdpateInteractionWidget(const FInteractableData* InteractableData) const;

protected:
	
	//========================================================================
	//PROPERTIES & VARIABLES
	//=========================================================================
	
	UPROPERTY()
	UMainMenu* MainMenuWidget;
	
	UPROPERTY()
	UInteractionWidget* InteractionWidget;

	
	
	//========================================================================
	//FUNCTIONS
	//=========================================================================

	virtual void BeginPlay() override;

};
