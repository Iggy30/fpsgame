// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/InventoryComponent.h"
#include "Items/ItemBase.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


UItemBase* UInventoryComponent::FindMathingItem(UItemBase* ItemIn) const
{
	if(ItemIn)
	{
		if(InventoryContents.Contains(ItemIn))
		{
			return ItemIn;
		}
	}
	return nullptr;
}

UItemBase* UInventoryComponent::FindNextItemID(UItemBase* ItemIn) const
{
	if(ItemIn)
	{
		//есть свой массив ,есть область действия,оператор доступа кторый получает тип элеманта в массиве и превращает ег ов указатель и мы называем его Result
		//Из массива получаем тип который является U элементной базой,а затем получаем результат ,а затем находим по ключю с права и смотрим на описапние этой находки
		if(const TArray<TObjectPtr<UItemBase>>::ElementType* Result = InventoryContents.FindByKey(ItemIn))
		{
			return *Result;
		}
	}
	return nullptr;
}

UItemBase* UInventoryComponent::FindNextParticalStack(UItemBase* ItemIn) const
{

	//метод поиска по Predicate
	if(const TArray<TObjectPtr<UItemBase>>::ElementType* Result =
		InventoryContents.FindByPredicate([&ItemIn](const UItemBase* InventoryItem)
		{
		return InventoryItem->ID == ItemIn->ID && !InventoryItem->IsFullItemStack();
		}
		))
	{
		return *Result;
	}
	return nullptr;

	
}

int32 UInventoryComponent::CalculateWeightAddAmount(UItemBase* ItemIn, int32 RequestedAddAmount)
{
	const int32 WeightMaxAddAmmount = FMath::FloorToInt((GetWeightCapacity() - InventoryTotalWeight) / ItemIn->GetItemSingleWeight());
	if(WeightMaxAddAmmount >= RequestedAddAmount)
	{
		return RequestedAddAmount;
	}
	return WeightMaxAddAmmount;
}

int32 UInventoryComponent::CalculateNumberForFullStack(UItemBase* StackableItem, int32 InitialRequestedAddAmount)
{
	const int32 AddAmountToMakeFullStack = StackableItem->NumericData.MaxStakSize - StackableItem->Quantity;

	return  FMath::Min(InitialRequestedAddAmount,AddAmountToMakeFullStack);
}

void UInventoryComponent::RemoveSingleInstanceOfItem(UItemBase* ItemToRemove)
{
	InventoryContents.RemoveSingle(ItemToRemove);
	OnInventoryUpdated.Broadcast();
}

int32 UInventoryComponent::RemoveAmountOfItem(UItemBase* ItemIn, int32 DesiredAmountToRemove)
{
	const int32 ActualAmountToRemove = FMath::Min(DesiredAmountToRemove,ItemIn->Quantity);

	ItemIn->SetQuantity(ItemIn->Quantity - ActualAmountToRemove);

	InventoryTotalWeight -= ActualAmountToRemove * ItemIn->GetItemSingleWeight();
	
	OnInventoryUpdated.Broadcast();

	return ActualAmountToRemove;
}

//Разделение существующего стека
void UInventoryComponent::SplitExistingStack(UItemBase* ItemIn, const int32 AmountTipSplit)
{
	//если содернжание имеет точку Num (мы хотим чтобы эта часть была ложной)
	if(InventoryContents.Num() + 1 <= InventorySlotsCapacity)
	{
		RemoveAmountOfItem(ItemIn,AmountTipSplit);
		AddNewItem(ItemIn,AmountTipSplit);
	}
}

FItemAddResult UInventoryComponent::HandleNonStackableItems(UItemBase* InputItem)
{
	// check if in the input has valid weight
	if(FMath::IsNearlyZero(InputItem->GetItemSingleWeight()) || InputItem->GetItemSingleWeight() < 0)
	{
		 return FItemAddResult::AddedNone(FText::Format
		 	(FText::FromString("Could not add {0} to the inventory.Item has invalid weight value!."),
		 		InputItem->TextData.Name));
	}
	// will the item weight overflow weight capacity
	if(InventoryTotalWeight + InputItem->GetItemSingleWeight() > GetWeightCapacity())
	{
		return FItemAddResult::AddedNone(FText::Format
			 (FText::FromString("Could not add {0} to the inventory.Item would overflow weight limit."),InputItem->TextData.Name));
	}
	//adding one more item would overflow slot capacity
	if(InventoryContents.Num() + 1 > InventorySlotsCapacity)
	{
		return FItemAddResult::AddedNone(FText::Format
			 (FText::FromString("Could not add {0} to the inventory.All inventory slots are full."),
			 	InputItem->TextData.Name));
	}

	AddNewItem(InputItem,1);
	return FItemAddResult::AddedAll(1,FText::Format
			 (FText::FromString("Successfully added a single {0} to the inventory."),
			 	InputItem->TextData.Name));
	
}

int32 UInventoryComponent::HandleStackableItems(UItemBase* ItemIn, int32 RequestedAddAmount)
{
	if(RequestedAddAmount <= 0 || FMath::IsNearlyZero(ItemIn->GetItemsStackWeight()))
	{
		//invalid item data
		return 0;
	}

	int32 AmountToDistibute = RequestedAddAmount;

	// check if the input item already exist in the inventory and is not a full stack
	UItemBase* ExistingItemStack = FindNextParticalStack(ItemIn);

	// distribute item stack over existing stack
	while (ExistingItemStack)
	{
		// calculate how many of the existing item would be needed to make the next full ctack
		const int32 AmountToMakeFullStack = CalculateNumberForFullStack(ExistingItemStack,AmountToDistibute);
		// calculate how many of the AmountToMakeFullStack can actually be carried based on the weight capacity
		const int32 WeightLimitAddAmount = CalculateWeightAddAmount(ExistingItemStack,AmountToMakeFullStack);

		// as long as the remaining amount of the otem does not overflow weight capacity
		if(WeightLimitAddAmount > 0)
		{
			// adjust the existing items stack quabtity and inventory total weight
			ExistingItemStack->SetQuantity(ExistingItemStack->Quantity + WeightLimitAddAmount);
			InventoryTotalWeight += ( ExistingItemStack->GetItemSingleWeight() * WeightLimitAddAmount);

			// adjust the count to be distributed
			AmountToDistibute -= WeightLimitAddAmount;

			ItemIn->SetQuantity(AmountToDistibute);

			// if max weight capacity is reashed,no need to run the loop again
			if(InventoryTotalWeight + ExistingItemStack->GetItemSingleWeight() > InventoryWeightCapacity)
			{
				OnInventoryUpdated.Broadcast();
				return RequestedAddAmount - AmountToDistibute;
			}
		}
		else if(WeightLimitAddAmount <= 0)
		{
			if (AmountToDistibute != RequestedAddAmount)
			{
				// this block will be reached if distributing an item across multiole stacks
				// and the weight limit is hit during that process
				OnInventoryUpdated.Broadcast();
				return RequestedAddAmount - AmountToDistibute;
			}
			return 0;
		}

		if(AmountToDistibute <= 0)
		{

			// all of the input item was distributed across
			OnInventoryUpdated.Broadcast();
			return RequestedAddAmount;
		}
		
		// check if there is still another calid partical stack of the item
		ExistingItemStack = FindNextParticalStack(ItemIn);
	}
	
	// no more partial stacks found, check if a new stack can be added
	if(InventoryContents.Num() + 1 <= InventorySlotsCapacity)
	{

		// attempt to add as many from the remaining item quantity that can fit inventory weight capacity
		const int32 WeightLimitAddAmount = CalculateWeightAddAmount(ItemIn,AmountToDistibute);

		if(WeightLimitAddAmount > 0)
		{
			// if there os still more otem to distribute, but weight limit hs been reached
			if(WeightLimitAddAmount < AmountToDistibute)
			{
				// adjust the input item and add a new stack with as many as can be held
				AmountToDistibute -= WeightLimitAddAmount;
				ItemIn->SetQuantity(AmountToDistibute);

				// create a copy since only a partial stack is being added
				AddNewItem(ItemIn->CreateItemCopy(),WeightLimitAddAmount);
				return RequestedAddAmount - AmountToDistibute;
			}

			// otherwise,the full remainder of the stack can be added
			AddNewItem(ItemIn,AmountToDistibute);
			return  RequestedAddAmount;
		}
		// reached if there is dree item slots,but no remaining weight capacity
		//OnInventoryUpdated.Broadcast();
		return RequestedAddAmount - AmountToDistibute;
	}
	// can only be reached if there is no existing stack and no extra capacity slots
	return 0;
	
}

FItemAddResult UInventoryComponent::HandleAddItem(UItemBase* InputItem)
{
	if(GetOwner())
	{
		const int32 InitialRequestedAddAmount = InputItem->Quantity;

		//handle non-stackable items
		if(!InputItem->NumericData.bIsStackable)
		{
			return HandleNonStackableItems(InputItem);
		}

		//handle stackable
		const int32 StackableAmountAdded = HandleStackableItems(InputItem, InitialRequestedAddAmount);

		//если сумма которую я добавил равна
		if(StackableAmountAdded == InitialRequestedAddAmount)
		{
			return FItemAddResult::AddedAll(InitialRequestedAddAmount,FText::Format(
				FText::FromString("Successfully added {0} {1} to the inventry."),
				InitialRequestedAddAmount,
				InputItem->TextData.Name));
		}
		//если маоя сумма была меньше первоначальной запрошенной суммы но больше 0 
		if(StackableAmountAdded < InitialRequestedAddAmount && StackableAmountAdded >0)
		{
			return FItemAddResult::AddedPartial(StackableAmountAdded,FText::Format(
				FText::FromString("Partial amount of {0} added to the inventory/ Number added = {1}"),
				InputItem->TextData.Name,
				StackableAmountAdded));
		}

		//если добавленная сумма меньше или равна 0,что никогда не должно быть меньше
		if(StackableAmountAdded <= 0)
		{
			return FItemAddResult::AddedNone(FText::Format(
				FText::FromString("Couldn't add {0} to the inventory. No remaining inventory slots, or invalid item."),
				InputItem->TextData.Name));
		}
	}
	check(false)
	return FItemAddResult::AddedNone(FText::FromString("TryAddItem fallthrough error. GetOwner() check somehow failed."));
}

void UInventoryComponent::AddNewItem(UItemBase* Item, const int32 AmountToAdd)
{
	UItemBase* NewItem;

	if(Item->bIsCopy || Item->bIsPickup)
	{

		// if the item is already a copy, or is a world picup
		NewItem = Item;
		NewItem->ResetItemFlags();
		
	}
	else
	{
		{
			// use when splitting or dragging to/from another inventory
			NewItem = Item->CreateItemCopy();
		}

		NewItem->OwningInventory = this;
		NewItem->SetQuantity(AmountToAdd);

		InventoryContents.Add(NewItem);
		InventoryTotalWeight += NewItem->GetItemsStackWeight();
		OnInventoryUpdated.Broadcast();
	}
}



