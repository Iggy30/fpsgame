// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/SHLauncherWeapon.h"
#include "Weapons/Projectile/Projectile_Grenade.h"


void ASHLauncherWeapon::FireStart()
{
	MakeShot();
}


void ASHLauncherWeapon::MakeShot()
{
	if(!GetWorld() || IsAmmoEmpty()) return;

	FVector TraceStart,TraceEnd;
	if(!GetTraceData(TraceStart,TraceEnd)) return;

	FHitResult HitResult;
	MakeHit(HitResult,TraceStart,TraceEnd);

	const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
	const FVector Diraction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();
	
	const FTransform SpawnTransform(FRotator::ZeroRotator,GetMuzzleWorldLocation());
	AProjectile_Grenade* Projectile_Grenade = GetWorld()->SpawnActorDeferred<AProjectile_Grenade>(ProgectileClass,SpawnTransform);
	if(Projectile_Grenade)
	{
		Projectile_Grenade->SetShotDiraction(Diraction);
		Projectile_Grenade->SetOwner(GetOwner());
		Projectile_Grenade->FinishSpawning(SpawnTransform);
	}
	// set projectile params
	DecriseAmmo();
	//UGameplayStatics::FinishSpawningActor(Projectile,SpawnTransform);
}
