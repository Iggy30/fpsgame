// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/SHRiffleWeapon.h"
#include "DrawDebugHelpers.h"
#include "AssetTypeActions/AssetDefinition_SoundBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "SHGame/Weapons/Projectile/ProjectileDefault.h"
#include "WorldPartition/ContentBundle/ContentBundleLog.h"



void ASHRiffleWeapon::FireStart()
{
	//UE_LOG(LogBaseWeapon,Display,TEXT("Fire!"));
	GetWorldTimerManager().SetTimer(ShotTimerHandle,this,&ASHRiffleWeapon::MakeShot,TimeBetweenShots,true);
	MakeShot();
	
}

void ASHRiffleWeapon::FireStop()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHandle);
}

void ASHRiffleWeapon::MakeShot()
{
	if(!GetWorld() || IsAmmoEmpty())
	{
		AmmoEmpty(NoAmmoSound);
		FireStop();
		
		return;
	}
	
	FVector TraceStart,TraceEnd;
	if(!GetTraceData(TraceStart,TraceEnd)) 
	{
		AmmoEmpty(NoAmmoSound);
		FireStop();
		return;
	}
	FHitResult HitResult;
	MakeHit(HitResult,TraceStart,TraceEnd);
	const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
	const FVector Diraction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();
	
	const FTransform SpawnTransform(FRotator::ZeroRotator,GetMuzzleWorldLocation());
	AProjectileDefault* ProjectileRifle = GetWorld()->SpawnActorDeferred<AProjectileDefault>(ProgectileClass,SpawnTransform);
	if(ProjectileRifle)
	{
		ProjectileRifle->SetShotDiraction(Diraction);
		ProjectileRifle->SetOwner(GetOwner());
		ProjectileRifle->FinishSpawning(SpawnTransform);
	}
	if(HitResult.bBlockingHit)
	{

		MakeDamage(HitResult);
		DrawDebugLine(GetWorld(),GetMuzzleWorldLocation(),HitResult.ImpactPoint,FColor::Red,false,3.0f,0,3.0f);
		
		DrawDebugSphere(GetWorld(),HitResult.ImpactPoint,10.f,24,FColor::Red,false,5.0f);

		UE_LOG(LogBlueprint,Display,TEXT("Bone: %s"),*HitResult.BoneName.ToString());
	}
	else
	{
		DrawDebugLine(GetWorld(),GetMuzzleWorldLocation(),TraceEnd,FColor::Red,false,3.0f,0,3.0f);
	}
	DecriseAmmo();
	SoundFXWeapon(FXFire,FireSound);
}

bool ASHRiffleWeapon::GetTraceData(FVector& TraceStart,FVector& TraceEnd) const
{
	FVector ViewLocation;
	FRotator ViewRotation;
	if(!GetPlayerViewPoint(ViewLocation,ViewRotation)) return false;

	TraceStart = ViewLocation;
	const auto HalfRad = FMath::DegreesToRadians(BulletSpread);
	const FVector ShootDiraction = FMath::VRandCone(ViewRotation.Vector(),HalfRad);
	TraceEnd = TraceStart + ShootDiraction * TraceMaxDistance;
	return true;
}

void ASHRiffleWeapon::MakeDamage(const FHitResult& HitResult)
{
	const auto DamageActor = HitResult.GetActor();
	if(!DamageActor) return;

	//DamageActor->TakeDamage(DamageAmount,FDamageEvent(),GetPlayerController(),this);
}