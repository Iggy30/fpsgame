// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/Projectile/Projectile_Grenade.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AProjectile_Grenade::AProjectile_Grenade()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	CollisionComponent->InitSphereRadius(5.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	SetRootComponent(CollisionComponent);

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");
	MovementComponent->InitialSpeed = 2000.0f;
	MovementComponent->ProjectileGravityScale = 0.0f;
}

// Called when the game starts or when spawned
void AProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();

	check(MovementComponent);
	check(CollisionComponent);
	MovementComponent->Velocity = ShotDiraction * MovementComponent->InitialSpeed;
	CollisionComponent->IgnoreActorWhenMoving(GetOwner(),true);
	CollisionComponent->OnComponentHit.AddDynamic(this,&AProjectile_Grenade::OnProjectileHit);
	SetLifeSpan(LifeSeconds);
	
}

void AProjectile_Grenade::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(!GetWorld()) return;
	MovementComponent->StopMovementImmediately();

	//Make Damage
	UGameplayStatics::ApplyRadialDamage(GetWorld(),
		DamageAmout,
		GetActorLocation(),
		DamageRadius,
		UDamageType::StaticClass(),
		{GetOwner()},
		this,
		GetController(),
		DoFullDamage);
	DrawDebugSphere(GetWorld(),GetActorLocation(),DamageRadius,24,FColor::Red,false,5.0f);
	FXExplose(ExploseFX,ExploseSound);
	Destroy();
}

AController* AProjectile_Grenade::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}

void AProjectile_Grenade::FXExplose(UParticleSystem* FX,USoundBase* ExploseS)
{
	if(FX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if(ExploseS)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExploseS, GetActorLocation(), GetActorRotation());
	}
}
