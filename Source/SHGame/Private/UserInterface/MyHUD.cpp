// Fill out your copyright notice in the Description page of Project Settings.


#include "UserInterface/MyHUD.h"
#include "UserInterface/MainMenu.h"
#include "UserInterface/Interaction/InteractionWidget.h"

AMyHUD::AMyHUD()
{
	
}


void AMyHUD::BeginPlay()
{
	Super::BeginPlay();

	if(MainMenuClass)
	{
		MainMenuWidget = CreateWidget<UMainMenu>(GetWorld(),MainMenuClass);
		MainMenuWidget->AddToViewport(5);
		MainMenuWidget->SetVisibility(ESlateVisibility::Collapsed);
	}

	if(InteractionWidgetClass)
	{
		InteractionWidget = CreateWidget<UInteractionWidget>(GetWorld(),InteractionWidgetClass);
		InteractionWidget->AddToViewport(-1);
		InteractionWidget->SetVisibility(ESlateVisibility::Collapsed);
	}
	
}


void AMyHUD::DisplayMenu()
{
	if(MainMenuWidget)
	{
		bIsMenuVisible = true;
		MainMenuWidget->SetVisibility((ESlateVisibility::Visible));
	}
}

void AMyHUD::HideMenu()
{
	if(MainMenuWidget)
	{
		bIsMenuVisible = false;
		MainMenuWidget->SetVisibility((ESlateVisibility::Collapsed));
	}
}

void AMyHUD::ToggleMenu()
{
	if(bIsMenuVisible)
	{
		HideMenu();

		const FInputModeGameOnly InputMode;
		GetOwningPlayerController()->SetInputMode(InputMode);
		GetOwningPlayerController()->SetShowMouseCursor(false);
	}
	else
	{
		{
			DisplayMenu();
			const FInputModeGameAndUI InputMode;
			GetOwningPlayerController()->SetInputMode(InputMode);
			GetOwningPlayerController()->SetShowMouseCursor(true);
		}
	}
}

void AMyHUD::ShowInteractionWidget() const
{
	if(InteractionWidget)
	{
	
		InteractionWidget->SetVisibility((ESlateVisibility::Visible));
	}
}

void AMyHUD::HideInteractionWidget() const
{
	if(InteractionWidget)
	{
		
		InteractionWidget->SetVisibility((ESlateVisibility::Collapsed));
	}
}

void AMyHUD::UdpateInteractionWidget(const FInteractableData* InteractableData) const
{

	if(InteractionWidget)
	{
		if(InteractionWidget->GetVisibility() ==ESlateVisibility::Collapsed)
		{
			InteractionWidget->SetVisibility(ESlateVisibility::Visible);
		}
	}

	InteractionWidget->UpdateWidget(InteractableData);
}

