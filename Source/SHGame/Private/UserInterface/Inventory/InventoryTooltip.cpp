// Fill out your copyright notice in the Description page of Project Settings.


#include "UserInterface/Inventory/InventoryTooltip.h"

#include "Components/TextBlock.h"
#include "UserInterface/Inventory/InventoryItemSlot.h"
#include "Items/ItemBase.h"
#include "SHGame/Weapons/WeaponDefault.h"


void UInventoryTooltip::NativeConstruct()
{
	Super::NativeConstruct();

	const UItemBase* ItemBeginHovered = InventorySlotBeginHovered->GetItemReference();

	switch (ItemBeginHovered->ItemType)
	{
	case EItemType::Armor:
		break;
	case EItemType::Ammo:
		ItemType->SetText(FText::FromString("Ammo Item"));
		DamageValue->SetVisibility(ESlateVisibility::Collapsed);
		UsageText->SetVisibility(ESlateVisibility::Collapsed);
		MaxStackSize->SetVisibility(ESlateVisibility::Collapsed);
		break;
	case EItemType::Weapon:
		ItemType->SetText(FText::FromString("Weapon Item"));
		DamageValue->SetVisibility(ESlateVisibility::Collapsed);
		UsageText->SetVisibility(ESlateVisibility::Collapsed);
		break;
	case EItemType::Clothes:
		ItemType->SetText(FText::FromString("Clothes Item"));
		DamageValue->SetVisibility(ESlateVisibility::Collapsed);
		UsageText->SetVisibility(ESlateVisibility::Collapsed);
		break;
	case EItemType::Food:
		break;
	case EItemType::Consumable:
		ItemType->SetText(FText::FromString("Consumable"));
		DamageValue->SetVisibility(ESlateVisibility::Collapsed);
		ArmorRating->SetVisibility(ESlateVisibility::Collapsed);
		MaxStackSize->SetVisibility(ESlateVisibility::Collapsed);
		
		break;
	case EItemType::Quest:
		break;
	case EItemType::Mundane:
		ItemType->SetText(FText::FromString("Mundane Item"));
		DamageValue->SetVisibility(ESlateVisibility::Collapsed);
		ArmorRating->SetVisibility(ESlateVisibility::Collapsed);
		UsageText->SetVisibility(ESlateVisibility::Collapsed);
		MaxStackSize->SetVisibility(ESlateVisibility::Collapsed);
		break;
	default: ;
	}

	ItemName->SetText(ItemBeginHovered->TextData.Name);
	DamageValue->SetText(FText::AsNumber(ItemBeginHovered->ItemStatistics.DamageValue));
	ArmorRating->SetText(FText::AsNumber(ItemBeginHovered->ItemStatistics.ArmorRating));
	UsageText->SetText(ItemBeginHovered->TextData.UsageText);
	ItemDescription->SetText(ItemBeginHovered->TextData.Description);
	MaxStackSize->SetText(FText::AsNumber(ItemBeginHovered->ItemStatistics.SellValue));
	StackWeight->SetText(FText::AsNumber(ItemBeginHovered->GetItemsStackWeight()));

	const FString WeightInfo =
		{"Weight:" + FString::SanitizeFloat(ItemBeginHovered->GetItemsStackWeight())};

	StackWeight->SetText(FText::FromString(WeightInfo));
	
	if(ItemBeginHovered->NumericData.bIsStackable)
	{
		const FString StackInfo =
			{"Max stack size:" + FString::FromInt(ItemBeginHovered->NumericData.MaxStakSize)};
		
		MaxStackSize->SetText(FText::AsNumber(ItemBeginHovered->NumericData.MaxStakSize));
	}
	else
	{
		{
			MaxStackSize->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}
