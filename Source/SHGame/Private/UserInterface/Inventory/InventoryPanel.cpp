// Fill out your copyright notice in the Description page of Project Settings.

#include "UserInterface/Inventory/InventoryPanel.h"
#include "SHGame/Characters/SHGameCharacter.h"
#include "UserInterface/Inventory/InventoryItemSlot.h"

#include "Components/TextBlock.h"
#include "Components/WrapBox.h"
#include "Items/ItemBase.h"
#include "UserInterface/Inventory/ItemDragDropOperation.h"


void UInventoryPanel::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	PlayerCharacter = Cast<ASHGameCharacter>(GetOwningPlayerPawn());
	if(PlayerCharacter)
	{
		InventoryReference = PlayerCharacter->GetInventory();
		if(InventoryReference)
		{
								//привязка дилегата
			InventoryReference->OnInventoryUpdated.AddUObject(this,&UInventoryPanel::RefresInventory);
			SetInfoText();
		}
	}
}

void UInventoryPanel::SetInfoText() const
{
	
	const FString WeightInfoValue{
	FString::SanitizeFloat(InventoryReference->GetInventoryTotalWeight()) + "/"
		+ FString::SanitizeFloat(InventoryReference->GetWeightCapacity())
	};

	const FString CapacityInfoValue{
		FString::FromInt(InventoryReference->GetInventoryContents().Num()) + "/"
			+ FString::FromInt(InventoryReference->GetSlotsCapacity())
	};

	 WeighInfo->SetText(FText::FromString(WeightInfoValue));
	CapacityInfo->SetText(FText::FromString(CapacityInfoValue));
	
	
}

void UInventoryPanel::RefresInventory()
{
	if(InventoryReference && InventorySlotClass)
	{
		//чистка перед тем как то то добавить в интвентарь
		InventoryWrapBox->ClearChildren();

		for(UItemBase* const& InventoryItem : InventoryReference->GetInventoryContents())
		{
			//создание слота инвентарного элемента вызывающий "создать виджет"
			UInventoryItemSlot* ItemSlot = CreateWidget<UInventoryItemSlot>(this,InventorySlotClass);
			ItemSlot->SetItemReference(InventoryItem);

			InventoryWrapBox->AddChildToWrapBox(ItemSlot);
			
		}
		SetInfoText();
	}
}

bool UInventoryPanel::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent,
	UDragDropOperation* InOperation)
{
	const UItemDragDropOperation* ItemDragDrop = Cast<UItemDragDropOperation>(InOperation);
	if ( ItemDragDrop->SourceItem && InventoryReference)
	{
		UE_LOG(LogTemp,Warning,TEXT("Deticated an item drop on InventoryWrapBox"))

		//returning true will stop the edrop operation at this widget
		return true;
	}
	// returning false will cause the drop operation  to fall through to underlying widgets (if any)
	return false;
}
