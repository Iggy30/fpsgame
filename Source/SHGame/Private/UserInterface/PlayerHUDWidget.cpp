// Fill out your copyright notice in the Description page of Project Settings.


#include "UserInterface/PlayerHUDWidget.h"
#include "SHGame/Characters/CharacterHealthComponent.h"
#include "SHGame/Characters/SHGameCharacter.h"

float UPlayerHUDWidget::GetHealthPercent() const
{
	const auto Player = GetOwningPlayerPawn();
	if(!Player) return 0.0f;

	const auto Component = Player->GetComponentByClass(UCharacterHealthComponent::StaticClass());
	const auto HealthComponent = Cast<UCharacterHealthComponent>(Component);
	if(!HealthComponent) return 0.0f;

	return HealthComponent->GetHealth();
	
}


