// Fill out your copyright notice in the Description page of Project Settings.


#include "World/PickUp.h"

#include "Items/ItemBase.h"

// Sets default values
APickUp::APickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>("PickupMesh");
	PickupMesh->SetSimulatePhysics(true);
	SetRootComponent(PickupMesh);
}



// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();

	InitializePickup(ItemQuantity);
	
}


void APickUp::InitializePickup(const int32 InQuanity)
{

	//Есть ли желаемый ИД товара и если она не пуста
	if(ItemDataTable && !DesiredItemID.IsNone())
	{
		const FItemData* ItemData = ItemDataTable->FindRow<FItemData>(DesiredItemID,DesiredItemID.ToString());

		ItemReference = NewObject<UItemBase>(this,UItemBase::StaticClass());

		//Инициалицзация наших ссылок на обьекты в Итем Дата
		ItemReference->ID = ItemData->ID;
		ItemReference->ItemType = ItemData->ItemType;
		ItemReference->ItemQuality = ItemData->ItemQuality;
		ItemReference->NumericData = ItemData->NumericData;
		ItemReference->TextData = ItemData->TextData;
		ItemReference->AssetData = ItemData->AssetData;
		//если наш размер стека равен 1 то это правда иначе не правда
		ItemReference->NumericData.bIsStackable = ItemData->NumericData.MaxStakSize > 1;
		//(проверка меньше или равна 0) тирнарный оператор - логическое условие,если истина то (1),двоеточие ,если ложно 
		InQuanity <= 0 ? ItemReference->SetQuantity(1) : ItemReference->SetQuantity(InQuanity);


		//Расположение сетки
		PickupMesh->SetStaticMesh(ItemData->AssetData.Mesh);		

		UpdateInteractableData();
	}
	
}

void APickUp::InitializeDrop(UItemBase* ItemDrop, const int32 InQuanity)
{
	ItemReference = ItemDrop;
	InQuanity <= 0 ? ItemReference->SetQuantity(1) : ItemReference->SetQuantity(InQuanity);
	ItemReference->NumericData.Weight = ItemDrop->GetItemSingleWeight();
	ItemReference->OwningInventory = nullptr;
	PickupMesh->SetStaticMesh(ItemDrop->AssetData.Mesh);

	UpdateInteractableData();
}

void APickUp::UpdateInteractableData()
{
	InstanceInteractableData.InteractableType = EInteractableType::Pickup;
	InstanceInteractableData.Action = ItemReference->TextData.InteractionText;
	InstanceInteractableData.Name = ItemReference->TextData.Name;
	InstanceInteractableData.Quantity = ItemReference->Quantity;

	//интерактивные данные равны интерактивным данным экхемпляра
	InteractableData = InstanceInteractableData;
	
}

void APickUp::BeginFocus()
{
	if(PickupMesh)
	{
		PickupMesh->SetRenderCustomDepth(true);
	}
}

void APickUp::EndFocus()
{
	if(PickupMesh)
	{
		PickupMesh->SetRenderCustomDepth(false);
	}
}


//Проверка,если персонаж игрока,то передается команда передать персонажу
void APickUp::Interact(ASHGameCharacter* PlayerCharacter)
{
	if(PlayerCharacter)
	{
		TakePickup(PlayerCharacter);
	}
}


void APickUp::TakePickup(const ASHGameCharacter* Taker)
{

	if(!IsPendingKillPending())
	{
		if(ItemReference)
		{
			if(UInventoryComponent* PlayerInventory = Taker->GetInventory())
			{
				const FItemAddResult AddResult = PlayerInventory->HandleAddItem(ItemReference);

				switch(AddResult.OperationResult)
				{
				case EItemAddResult::IAR_NoItemAdded:					
					break;
				case EItemAddResult::IAR_PartialAmountItemAdded:
					UpdateInteractableData();
					Taker->UpdateInteractionWidget();
					
					break;
				case EItemAddResult::IAR_AllItemAdded:
					Destroy();
					break;
					
				}
				UE_LOG(LogTemp,Warning,TEXT("%s"), *AddResult.ResultMessage.ToString());
			}
			else
			{
				UE_LOG(LogTemp,Warning,TEXT("Player inventory component is null!"));
			}
			
		}
		else
		{
			UE_LOG(LogTemp,Warning,TEXT("Pickup internal item reference was somehow null!"));
		}
	}
	
}

void APickUp::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	const FName ChangePropertyName = PropertyChangedEvent.Property ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	//проверка,соответствует ли он нужному индефикатору в клкассе PickUp
	if(ChangePropertyName == GET_MEMBER_NAME_CHECKED(APickUp,DesiredItemID)) //5 часть 46 m
	{
		if(ItemDataTable)
		{
			if(const FItemData* ItemData = ItemDataTable->FindRow<FItemData>(DesiredItemID,DesiredItemID.ToString()))
			{
				PickupMesh->SetStaticMesh(ItemData->AssetData.Mesh);
			}
		}
	}
}

