// Fill out your copyright notice in the Description page of Project Settings.


#include "World/InterfaceActorTest.h"

// Sets default values
AInterfaceActorTest::AInterfaceActorTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");

	SetRootComponent(Mesh);
	
}

// Called when the game starts or when spawned
void AInterfaceActorTest::BeginPlay()
{
	Super::BeginPlay();
	InteractableData = InstanceInteractableData;
}

// Called every frame
void AInterfaceActorTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInterfaceActorTest::BeginFocus()
{
	if(Mesh)
	{
		Mesh->SetRenderCustomDepth(true);
	}
}

void AInterfaceActorTest::EndFocus()
{
	if(Mesh)
	{
		Mesh->SetRenderCustomDepth(false);
	}
}

void AInterfaceActorTest::BeginInteract()
{
	UE_LOG(LogTemp,Warning,TEXT("Calling BeginInteract ovveride on interface test actor."));
}

void AInterfaceActorTest::EndInteract()
{
	UE_LOG(LogTemp,Warning,TEXT("Calling EndInteract ovveride on interface test actor."));
}

void AInterfaceActorTest::Interact(ASHGameCharacter* PlayerCharacter)
{
	UE_LOG(LogTemp,Warning,TEXT("Calling Interact ovveride on interface test actor."));
}

