// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/ItemBase.h"
#include "SHGame/Weapons/WeaponDefault.h"
#include "SHGame/Weapons/Projectile/ProjectileDefault.h"
#include  "Components/InventoryComponent.h"

UItemBase::UItemBase() : bIsCopy(false),bIsPickup(false)
{
	
}

void UItemBase::ResetItemFlags()
{
	bIsCopy = false;
	bIsPickup = false;
	
}

UItemBase* UItemBase::CreateItemCopy()
{
	//Чтобы создать новый обьект из элемента статик класс
	UItemBase* ItemCopy = NewObject<UItemBase>(StaticClass());
	//Создание новой базы товара
	ItemCopy->ID = this->ID;
	ItemCopy->Quantity= this->Quantity;
	ItemCopy->ItemQuality= this->ItemQuality;
	ItemCopy->ItemType= this->ItemType;
	ItemCopy->WeaponType=this->WeaponType;
	ItemCopy->ArmorType=this->ArmorType;
	ItemCopy->TextData = this->TextData;
	ItemCopy->NumericData = this->NumericData;
	ItemCopy->ItemStatistics = this->ItemStatistics;
	ItemCopy->AssetData= this->AssetData;

	//ItemCopy->NumericData.bIsStackable = (this->NumericData.MaxStakSize >1) ? true : false;
	ItemCopy->bIsCopy = true;
	return ItemCopy;
}

void UItemBase::SetQuantity(const int32 NewQuantity)
{
	//Если не совпадает с нашим текущем количеством
	if(NewQuantity != Quantity)
	{
		Quantity = FMath::Clamp(NewQuantity,0,NumericData.bIsStackable ? this->NumericData.MaxStakSize: 1);

		if(this->OwningInventory)
		{
			if(this->Quantity<= 0)
			{
				this->OwningInventory->RemoveSingleInstanceOfItem(this);
			}

		}
		else
		{
			{
				UE_LOG(LogTemp,Warning,TEXT("ItemBase OwningInventory was null (item may be a pickup)."));
			}
		}
	}
}

void UItemBase::Use(ASHGameCharacter* Character)
{
	
}
