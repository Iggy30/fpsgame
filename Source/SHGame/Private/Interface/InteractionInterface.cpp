// Fill out your copyright notice in the Description page of Project Settings.


#include "Interface/InteractionInterface.h"
#include "SHGame/Characters/SHGameCharacter.h"
// Add default functionality here for any IInteractionInterface functions that are not pure virtual.
void IInteractionInterface::BeginFocus()
{
}

void IInteractionInterface::EndFocus()
{
}

void IInteractionInterface::BeginInteract()
{
}

void IInteractionInterface::EndInteract()
{
}

void IInteractionInterface::Interact(ASHGameCharacter* PlayerCharacter)
{
}
