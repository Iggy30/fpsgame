// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Chaos/ChaosEngineInterface.h"
#include "Containers/EnumAsByte.h"
#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicsSettingsCore.h"
#include "ItemDataStructs.generated.h"



//������������ �������� ��������
UENUM()
enum class EItemQuality:uint8
{
	Shoddy UMETA(DisplayName = "Shoddy"),
	Common UMETA(DisplayName = "Common"),
	Quality UMETA(DisplayName = "Quality"),
	Masterwork UMETA(DisplayName = "Masterwork"),
	Grandmaster UMETA(DisplayName = "Grandmaster")

};

//��� ��������
UENUM()
enum class EItemType:uint8
{
	Armor UMETA(DisplayName = "Armor"),
	Ammo UMETA(DisplayName = "Ammo"),
	Weapon UMETA(DisplayName = "Weapon"),
	Clothes UMETA(DisplayName = "Clothes"),
	Food UMETA(DisplayName = "Food"),
	Consumable UMETA(DisplayName = "Consumable"),
	Quest UMETA(DisplayName = "Quest"),
	Mundane UMETA(DisplayName = "Mundane")
	
};


//���������� �� ��������� ���������
USTRUCT()
struct FItemStatistics
{
	GENERATED_BODY()
		
	UPROPERTY(EditAnywhere)
	float ArmorRating;

	UPROPERTY(EditAnywhere)
	float DamageValue;

	UPROPERTY(EditAnywhere)
	float RestorationAmount;

	UPROPERTY(EditAnywhere)
	float SellValue;
};

//��������� ������ �� ���������
USTRUCT()
struct FItemTextData
{
	GENERATED_BODY()
		
	//�������� ��������
	UPROPERTY(EditAnywhere)
	FText Name;
	//�������� ��������
	UPROPERTY(EditAnywhere)
	FText Description;
	//������� ��� ��������������
	UPROPERTY(EditAnywhere)
	FText InteractionText;
	//��������� �������
	UPROPERTY(EditAnywhere)
	FText UsageText;

};

//�������� ������
USTRUCT()
struct FItemNumericData
{
	GENERATED_BODY()
		
	//���
	UPROPERTY(EditAnywhere)
	float Weight;

	//������������ ���
	UPROPERTY(EditAnywhere)
	int32 MaxStakSize;

	//����� �� ��� ���������
	UPROPERTY()
	bool bIsStackable;

};


USTRUCT()
struct FItemAssetData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AProjectileDefault> ProjectileClass = nullptr;

	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;
	

	UPROPERTY(EditAnywhere)
	UTexture2D* Icon;


	UPROPERTY(EditAnywhere)
	UStaticMesh* Mesh;

};
USTRUCT()
struct FItemData:public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Item Data")
	FName ID;

	UPROPERTY(EditAnywhere,Category= "Item Data")
	EItemType ItemType;

	UPROPERTY(EditAnywhere, Category = "Item Data")
	EItemQuality ItemQuality;

	UPROPERTY(EditAnywhere, Category = "Item Data")
	FItemStatistics ItemStatistics;

	UPROPERTY(EditAnywhere, Category = "Item Data")
	FItemTextData TextData;

	UPROPERTY(EditAnywhere, Category = "Item Data")
	FItemNumericData NumericData;

	UPROPERTY(EditAnywhere, Category = "Item Data")
	FItemAssetData AssetData;

};

 UENUM(BlueprintType)
 enum class EMovementState : uint8
 {
 	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
 	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "SprintRun State"),
	Crouch_State UMETA(DisplayNName = "Crouch State"),
 	CrouchAim_State UMETA(DisplayName = "CrouchAim State"),
 	CrouchRun_State UMETA(DisplayName = "CrouchRun State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "ShotGun"),
	SniperRifle UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher"),
	PistolType UMETA(DisplayName = "Pistol"),
	OneHandType UMETA(DisplayName = "OneHandMeele"),
	TwoHandType UMETA(DisplayName = "TwoHandMeele")
	
};

UENUM(BlueprintType)
enum class EArmorType : uint8
{
		Head UMETA(DisplayName = "Head"),
		Chest UMETA(DisplayName = "Chest"),
		Legs UMETA(DisplayName = "Legs"),
		Hands UMETA(DisplayName = "Hands"),
		Shoulders UMETA(DisplayName = "Shoulders"),
		Feet UMETA(DisplayName = "Feet")
};


USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float CrouchSpeedNormal = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float CrouchRunSpeedRun = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float RunSpeedNormal = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float SprintRunSpeedRun = 900.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UParticleSystem* ProjectileTrailFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileTrailFxOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed = 5000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileIMaxSpeed = 2000.0f;
	//material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	USoundBase* HitSound = nullptr;

	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	//TSubclassOf<UStateEffect> Effect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	UParticleSystem* ExploseFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	USoundBase* ExploseSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	bool bIsLikeBomb = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ExploseMaxDamage = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ExplodeFalloffCoef = 1.0f;
	//Timer add
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionReduction = .3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionReduction = 0.1f;
};


USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Anim Char")
		UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
		UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
		UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
		UAnimMontage* AnimWeaponFire = nullptr;
		
};


USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "DropMesh")
		UStaticMesh* DropMesh = nullptr;
	//0.0f imediately drop
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float DropMeshTime = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float DropMeshLifeTime = 5.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FTransform DropMeshOffset = FTransform();
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FVector DropMeshInpulseDir = FVector(0.0f);
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float PowerInpuls = 0.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float ImpulseRandomDispersion = 0.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float CustomMass = 0.0f;


};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
	//TSubclassOf<class AMyWeapon> WeaponData = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound ")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound ")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX ")
	UParticleSystem* EffectFireWeapon = nullptr;
	//if null use trace logic (TSubclassOf<class AProjectileDefault> Projectile = nullptr)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile ")
	FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
	float DistacneTrace = 2000.0f;
	//one decal on all?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect ")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	FAnimationWeaponInfo AnimWeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
	FDropMeshInfo ClipDropMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
	FDropMeshInfo ShellBullets;

	
};

USTRUCT(BlueprintType)
struct FAddicionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 Round = 10;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon Stats")
	int32 Clips;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Weapon Stats")
	bool Infinite;
	
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "WeaponSlot")
	FAddicionalWeaponInfo AdditionalInfo;
};


UCLASS()
class SHGAME_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:

	//UFUNCTION(BlueprintCallable)
	//static void AddEffectBySurfaceType(AActor* TakeEffectActor,FName NameBoneHit, TSubclassOf<UStateEffect> AddEffectClass, EPhysicalSurface SurfaceType);

};
